package Selenium.Pock;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
	WebDriver driver = null;
    /**
     * Rigorous Test :-)
     * @throws InterruptedException 
     */
  @Test
    public  void main(  ) throws InterruptedException
    {
        System.out.println( "Hello World!" );
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
	
     options.setHeadless(true);
     /* 
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		*/
		
		driver = new ChromeDriver(options); 
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get("https://www.realmadrid.com/fr");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"menu-sec-4\"]")).click();
		Thread.sleep(2000);

		
		String text = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[1]/div/h1")).getText();
		System.out.println("yes papa "+text);
      
    }
  
  
  @AfterTest
  public void quit()
  {
  	//driver.quit();
  }
  
  
}
